<?php

namespace Logicamente\DocumentorBundle\Command;

use ArrayIterator;
use ReflectionClass;
use Doctrine\Common\Collections\ArrayCollection;
use Logicamente\DocumentorBundle\Controller\DocumentorController;
use Logicamente\DocumentorBundle\Entity\MetaClass;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Classe de Comando do Documentor
 */
class DocumentorCommand extends Command
{

    /** @var Application */
    private $application;
    /** @var KernelInterface */
    private $kernel;
    /** @var string */
    private $namespace;
    /** @var string */
    private $outputPath;
    /** @var bool */
    private $full;
    /** @var ArrayCollection */
    private $classes;
    /** @var string */
    private $sourceDirectory;
    /** @var ArrayIterator */
    private $files;
    /** @var OutputInterface */
    private $output;
    /** @var DocumentorController */
    private $documentorController;

    public function __construct()
    {
        parent::__construct();
        $this->files = new ArrayIterator();
        $this->classes = new ArrayCollection();
        $this->documentorController = new DocumentorController();
    }

    protected function configure()
    {
        $this
            ->setName('documentor:generate')
            ->setDescription('Gera documentação.')
            ->setHelp('Gera um arquivo LaTeX documentando as classes do projeto.')
            ->addArgument('namespace', InputArgument::OPTIONAL, 'Namespace dos arquivos na pasta src.')
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Diretório de saída do arquivo tex.')
            ->addOption('full', 'f', InputOption::VALUE_NONE, 'Carrega todas as classes de herança, implementação, traits, retornos e parâmetros.')
            ->addOption('umlet', 'u', InputOption::VALUE_NONE, 'Cria descrições em um arquivo txt para serem utilizados no UMLet.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Application */
        $this->application = $this->getApplication();
        $this->kernel = $this->application->getKernel();
        $this->documentorController->setContainer($this->kernel->getContainer());
        $this->namespace = $input->getArgument('namespace') ?? 'App';
        $this->outputPath = $this->kernel->getProjectDir() . DIRECTORY_SEPARATOR . $input->getOption('output');
        $this->full = $input->getOption('full');
        $this->sourceDirectory = $this->kernel->getProjectDir() . DIRECTORY_SEPARATOR . 'src';
        $this->output = $output;
        $output->writeln("\nPasta do código-fonte: $this->sourceDirectory\n");
        $this->loadAllFiles($this->sourceDirectory);
        $this->loadAllClasses();
        $this->documentorController->generate($this->classes, $this->outputPath);
        if($input->getOption('umlet')) {
            $this->documentorController->generateUmlet($this->classes, $this->outputPath);
        }
        return 0;
    }

    private function loadAllFiles(string $directory)
    {
        $this->output->writeln("\n\nPercorrendo a pasta: $directory\n");
        foreach (glob($directory . DIRECTORY_SEPARATOR . '*') as $item) {
            if (strpos($item, 'Migrations') === false) {
                if (is_dir($item)) $this->loadAllFiles($item);
                $file = explode('.', $item);
                if (end($file) === 'php') {
                    $this->files->append($item);
                    $this->output->writeln("Arquivo encontrado: $item");
                }
            }
        }
    }

    private function loadAllClasses()
    {
        $this->output->writeln("\n\nCarregando classes\n");
        foreach ($this->files as $file) {
            $file = str_replace([$this->sourceDirectory, '.php', '/'], ['', '', '\\'], $file);
            $className = "$this->namespace$file";
            $reflectedClass = new \ReflectionClass($className);
            $this->loadSelfParentsInterfacesAndTraits(new MetaClass($reflectedClass));
            $this->output->writeln("Classe carregada: $className");
        }
    }

    private function loadSelfParentsInterfacesAndTraits(MetaClass $class)
    {
        if (!$this->isClassAdded($class)) {
            $this->classes->add($class);
            if ($this->full) {
                if (!empty($class->getExtends()))
                    $this->loadSelfParentsInterfacesAndTraits($class->getExtends());
                foreach ($class->getImplements() as $interface)
                    $this->loadSelfParentsInterfacesAndTraits($interface);
                foreach ($class->getTraits() as $traits)
                    $this->loadSelfParentsInterfacesAndTraits($traits);
                foreach ($class->getProperties() as $property)
                    if (class_exists($property->getType()))
                        $this->loadSelfParentsInterfacesAndTraits(new MetaClass(new ReflectionClass($property->getType())));
                foreach ($class->getMethods() as $method) {
                    if (class_exists($method->getReturnType()))
                        $this->loadSelfParentsInterfacesAndTraits(new MetaClass(new ReflectionClass($method->getReturnType())));
                    foreach ($method->getParameters() as $paramater)
                        if (class_exists($paramater->getType()))
                            $this->loadSelfParentsInterfacesAndTraits(new MetaClass(new ReflectionClass($paramater->getType())));
                }
            }
        }
    }

    private function isClassAdded(MetaClass $class)
    {
        foreach ($this->classes as $classAdded) {
            if ($classAdded->getName() === $class->getName() && $classAdded->getNamespace() === $class->getNamespace())
                return true;
        }
        return false;
    }
}
