<?php

namespace Logicamente\DocumentorBundle\Controller;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

/**
 * Controla as anotações feitas especificamente para Doctrine
 */
class DoctrineAnnotationsController
{

    /** @var DoctrineAnnotationsController */
    private static $instance;

    /** @var AnnotationReader */
    private $annotationReader;

    public static function createInstance()
    {
        if (is_null(self::$instance))
            self::$instance = new self;
        return self::$instance;
    }

    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();
        $this->annotationReader->addGlobalIgnoredName('required');
        $this->annotationReader->addGlobalIgnoredName('implements');
        $this->annotationReader->addGlobalIgnoredName('inherits');
        $loader = require __DIR__ . '/../../../../vendor/autoload.php';
        AnnotationRegistry::registerLoader([$loader, 'loadClass']);
    }

    public function getAnnotationReader(): AnnotationReader
    {
        return $this->annotationReader;
    }
}
