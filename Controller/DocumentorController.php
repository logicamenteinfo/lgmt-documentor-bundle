<?php

namespace Logicamente\DocumentorBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Logicamente\DocumentorBundle\Entity\MetaClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DocumentorController extends AbstractController
{

    /**
     * @param MetaClass[] $classes Classes para gerar documentação     
     */
    public function generate(ArrayCollection $classes, string $outputPath)
    {
        $twig = $this->get('twig');
        $twig->getExtension('Twig_Extension_Core')->setEscaper(
            'latex',
            function ($twig, $string, $charset) {
                $search = ['\\', '_', '%', '&', '#', '{', '}', '<br>', '\\n'];
                $replace = ['$\\backslash$', '\\_', '\\%', '\\&', '\\#', '\\{', '\\}', '\\newline', '\\newline'];
                $string = str_replace($search, $replace, $string);
                $string = preg_replace('/(?<!\\\\backslash)\$(?!\\\\backslash)/i', '\\\\$', $string);
                return $string;
            }
        );
        $fp = fopen($outputPath . DIRECTORY_SEPARATOR . 'classes.tex', 'w+');
        fwrite($fp, $this->renderView('@LogicamenteDocumentor/classes.tex.twig', [
            'classes' => $classes
        ]));
        fclose($fp);
    }

    public function generateUmlet(ArrayCollection $classes, string $outputPath)
    {
        $fp = fopen($outputPath . DIRECTORY_SEPARATOR . 'umlet.txt', 'w+');
        fwrite($fp, $this->renderView('@LogicamenteDocumentor/umlet.txt.twig', [
            'classes' => $classes
        ]));
        fclose($fp);
    }
}
