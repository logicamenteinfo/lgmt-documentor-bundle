<?php

namespace Logicamente\DocumentorBundle\Entity;

/**
 * Tipos de classe existentes em PHP
 */
class MetaClassType
{

    const T_REGULAR = 'class';
    const T_ABSTRACT = 'abstract';
    const T_TRAIT = 'trait';
    const T_INTERFACE = 'interface';
    const T_FINAL = 'final';
}
