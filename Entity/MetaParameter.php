<?php

namespace Logicamente\DocumentorBundle\Entity;

use ReflectionException;
use ReflectionParameter;

class MetaParameter
{

    /** @var ReflectionParameter */
    protected $reflected;
    /** @var string */
    protected $name;
    /** @var string */
    protected $description;
    /** @var string */
    protected $type;
    /** @var mixed */
    protected $defaultValue;
    /** @var bool */
    protected $required;

    public function __construct(ReflectionParameter $parameter, string $type = '', string $description = '')
    {
        $this->reflected = $parameter;
        $this->name = $parameter->getName();
        $this->required = !$parameter->isOptional();
        $this->type = $type;
        $this->description = $description;
        $this->loadType();
        $this->loadDefaultValue();
    }

    private function loadType()
    {
        if ($this->reflected->hasType()) {
            $this->type = $this->reflected->getType()->__toString();
        }
    }

    private function loadDefaultValue()
    {
        try {
            $this->defaultValue = !$this->required ? $this->reflected->getDefaultValue() : null;
            if (is_array(($this->defaultValue))) {
                $this->defaultValue = str_replace(',', ', ', json_encode($this->defaultValue));
            } else if (is_string($this->defaultValue)) {
                $this->defaultValue = "'$this->defaultValue'";
            }
        } catch (ReflectionException $e) {
        }
    }

    /**
     * Get the value of name
     *
     * @return  mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of type
     *
     * @return  mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the value of defaultValue
     *
     * @return  mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Get the value of required
     *
     * @return  mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Get the value of description
     *
     * @return  mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
}
