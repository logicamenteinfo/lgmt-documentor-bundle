<?php

namespace Logicamente\DocumentorBundle\Entity;

use ArrayIterator;
use ReflectionClass;

/**
 * Determina as características de uma classe e carrega seus dados a partir de um `ReflectionClass`
 */
class MetaClass extends AbstractHasDocBlock
{

    /** @var ReflectionClass */
    protected $reflected;
    /** @var string */
    protected $namespace;
    /** @var string */
    protected $type;
    /** @var string */
    protected $name;
    /** @var MetaClass */
    protected $extends;
    /** @var ArrayIterator */
    protected $implements;
    /** @var ArrayIterator */
    protected $traits;
    /** @var ArrayIterator */
    protected $constants;
    /** @var ArrayIterator */
    protected $properties;
    /** @var ArrayIterator */
    protected $methods;
    /** @var ArrayIterator */
    protected $capabilities;

    public function __construct(ReflectionClass $class)
    {
        parent::__construct($class);
        $this->namespace = $class->getNamespaceName();
        $this->name = $class->getShortName();
        $this->loadType();
        $this->loadParent();
        $this->loadInterfaces();
        $this->loadTraits();
        $this->loadConstants();
        $this->loadProperties();
        $this->loadMethods();
        $this->loadResources();
    }



    private function loadType()
    {
        if ($this->reflected->isAbstract())
            $this->type = MetaClassType::T_ABSTRACT;
        else if ($this->reflected->isInterface())
            $this->type = MetaClassType::T_INTERFACE;
        else if ($this->reflected->isFinal())
            $this->type = MetaClassType::T_FINAL;
        else if ($this->reflected->isTrait())
            $this->type = MetaClassType::T_TRAIT;
        else
            $this->type = MetaClassType::T_REGULAR;
    }

    private function loadParent()
    {
        $parent = $this->reflected->getParentClass();
        if ($parent !== false)
            $this->extends = new MetaClass($parent);
    }

    private function loadInterfaces()
    {
        $this->implements = new ArrayIterator();
        foreach ($this->reflected->getInterfaces() as $interface) {
            $this->implements->append(new MetaClass($interface));
        }
    }

    private function loadTraits()
    {
        $this->traits = new ArrayIterator();
        foreach ($this->reflected->getTraits() as $trait) {
            $this->traits->append(new MetaClass($trait));
        }
    }

    private function loadConstants()
    {
        $this->constants = new ArrayIterator();
        foreach ($this->reflected->getConstants() as $name => $value) {
            $this->constants->append(new MetaConstant($name, $value));
        }
    }

    private function loadProperties()
    {
        $this->properties = new ArrayIterator();
        foreach ($this->reflected->getProperties() as $property) {
            if ($property->getDeclaringClass() == $this->reflected)
                $this->properties->append(new MetaProperty($property));
        }
    }

    private function loadMethods()
    {
        $this->methods = new ArrayIterator();
        foreach ($this->reflected->getMethods() as $method) {
            if ($method->getDeclaringClass() == $this->reflected)
                $this->methods->append(new MetaMethod($method));
        }
    }

    private function loadResources()
    {
        $this->capabilities = new ArrayIterator();
        if ($this->reflected->isAnonymous()) {
            $this->capabilities->append(MetaClassCapabilities::T_ANONYMOUS);
        }
        if ($this->reflected->isCloneable()) {
            $this->capabilities->append(MetaClassCapabilities::T_CLONEABLE);
        }
        if ($this->reflected->isInstantiable()) {
            $this->capabilities->append(MetaClassCapabilities::T_INSTANTIABLE);
        }
        if ($this->reflected->isInternal()) {
            $this->capabilities->append(MetaClassCapabilities::T_INTERNAL);
        }
        if ($this->reflected->isIterable()) {
            $this->capabilities->append(MetaClassCapabilities::T_ITERABLE);
        }
    }

    /**
     * Get the value of namespace
     *
     * @return  string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * Get the value of type
     *
     * @return  string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getNameClear(): string
    {
        return str_replace('_', '', $this->name);
    }

    /**
     * Get the value of extends
     *
     * @return  MetaClass
     */
    public function getExtends(): ?MetaClass
    {
        return $this->extends;
    }

    /**
     * Get the value of implements
     *
     * @return  MetaClass[]
     */
    public function getImplements(): ArrayIterator
    {
        return $this->implements;
    }

    /**
     * Get the value of traits
     *
     * @return  MetaClass[]
     */
    public function getTraits(): ArrayIterator
    {
        return $this->traits;
    }

    /**
     * Get the value of constants
     *
     * @return  MetaConstant[]
     */
    public function getConstants(): ArrayIterator
    {
        return $this->constants;
    }

    /**
     * Get the value of properties
     *
     * @return  MetaProperty[]
     */
    public function getProperties(): ArrayIterator
    {
        return $this->properties;
    }

    /**
     * Get the value of methods
     *
     * @return  MetaMethod[]
     */
    public function getMethods(): ArrayIterator
    {
        return $this->methods;
    }

    /**
     * Get the value of capabilities
     *
     * @return  string[]
     */
    public function getCapabilities(): ArrayIterator
    {
        return $this->capabilities;
    }
}
