<?php

namespace Logicamente\DocumentorBundle\Entity;

use ReflectionProperty;

/**
 * Determina as características de uma propriedade e carrega seus dados a partir de um `ReflectionProperty`
 */
class MetaProperty extends AbstractHasDocBlock
{

    /** @var ReflectionProperty */
    protected $reflected;
    /** @var string */
    protected $name;
    /** @var string */
    protected $type;
    /** @var string */
    protected $defaultValue;
    /** @var string */
    protected $visibility;
    /** @var bool */
    protected $isStatic;

    public function __construct(ReflectionProperty $property)
    {
        parent::__construct($property);
        $this->name = $property->getName();
        $this->isStatic = $property->isStatic();
        $this->loadDefaultValue();
        $this->loadType();
        $this->loadVisibility();
    }

    private function loadType()
    {        
        $var = !is_null($this->docblock) ? $this->docblock->getTagsByName('var') : null;
        if (!empty($var))
            $this->type = $var[0];
        else if(!empty($this->reflected->getType()))
            $this->type = $this->reflected->getType()->__toString();
        else if (!empty($this->defaultValue))
            $this->type = gettype($this->defaultValue);
    }

    private function loadDefaultValue()
    {
        $class = $this->reflected->getDeclaringClass();
        $defaultProp = $class->getDefaultProperties();
        $this->defaultValue = $defaultProp[$this->name];
        if (is_array(($this->defaultValue))) {
            $this->defaultValue = str_replace(',', ', ', json_encode($this->defaultValue));
        }
    }

    private function loadVisibility()
    {
        $this->visibility =
            $this->reflected->isPrivate() ? 'privado' :
            $this->reflected->isProtected() ? 'protegido' :
            'público';
    }

    /**
     * Get the value of isStatic
     *
     * @return  mixed
     */
    public function isStatic()
    {
        return $this->isStatic;
    }

    /**
     * Get the value of name
     *
     * @return  mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of type
     *
     * @return  mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the value of defaultValue
     *
     * @return  mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Get the value of visibility
     *
     * @return  mixed
     */
    public function getVisibility()
    {
        return $this->visibility;
    }
}
