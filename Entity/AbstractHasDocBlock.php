<?php

namespace Logicamente\DocumentorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;
use RuntimeException;
use InvalidArgumentException;
use Doctrine\ORM\Mapping\Annotation;
use Logicamente\DocumentorBundle\Controller\DoctrineAnnotationsController;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlock\Tag;
use phpDocumentor\Reflection\DocBlockFactory;
use Symfony\Component\Routing\Annotation\Route;

abstract class AbstractHasDocBlock
{

    /** @var ReflectionClass|ReflectionMethod|ReflectionProperty */
    protected $reflected;
    /** @var string */
    protected $description;
    /** @var Tag[] */
    protected $tags;
    /** @var Annotation[] */
    protected $doctrineAnnotations;
    /** @var DocBlock */
    protected $docblock;

    public function __construct($reflected)
    {
        $this->reflected = $reflected;
        $this->doctrineAnnotations = new ArrayCollection();
        $this->loadDescriptionAndTags();
        $this->loadDoctrineAnnotations();
    }

    private function loadDescriptionAndTags()
    {
        $docblock = $this->reflected->getDocComment();
        if (!empty($docblock)) {
            $factory = DocBlockFactory::createInstance();            
            try {
                $this->docblock = $factory->create($docblock);
                $this->description = $this->docblock->getSummary();
                $this->tags = $this->docblock->getTags();
            } catch (InvalidArgumentException $e) {
            } catch (RuntimeException $e) {
            }
        }
    }

    private function loadDoctrineAnnotations()
    {
        preg_match_all('/(@(ORM\\\\\\w+|Route))(\\(.+\\))?/', $this->reflected->getDocComment(), $matches);
        if (!empty($matches[0])) {
            for ($i = 0; $i < count($matches[0]); $i++) {
                $this->doctrineAnnotations->add(['name' => $matches[2][$i], 'value' => $matches[3][$i]]);
            }
        }
    }

    /**
     * Get the value of description
     *
     * @return  mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the value of tags
     *
     * @return  mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get the value of doctrineAnnotations
     *
     * @return  mixed
     */
    public function getDoctrineAnnotations()
    {
        return $this->doctrineAnnotations;
    }
}
