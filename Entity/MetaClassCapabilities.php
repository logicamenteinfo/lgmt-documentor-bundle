<?php

namespace Logicamente\DocumentorBundle\Entity;

class MetaClassCapabilities {

    const T_ANONYMOUS = 'anonymous';
    const T_CLONEABLE = 'cloneable';
    const T_INSTANTIABLE = 'instantiable';
    const T_INTERNAL = 'internal';
    const T_ITERABLE = 'iterable';

}