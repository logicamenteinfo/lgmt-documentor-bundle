<?php

namespace Logicamente\DocumentorBundle\Entity;

class MetaConstant
{

    protected $name;
    protected $type;
    protected $value;

    public function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->value = $value;
        $this->type = gettype($value);
    }

    /**
     * Get the value of name
     *
     * @return  mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of type
     *
     * @return  mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the value of value
     *
     * @return  mixed
     */
    public function getValue()
    {
        $value = (is_string($this->value)) ? "'$this->value'" : $this->value;
        if (is_array($value)) {
            $value = json_encode($value);
        }
        return $value;
    }
}
