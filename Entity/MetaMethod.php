<?php

namespace Logicamente\DocumentorBundle\Entity;

use ArrayIterator;
use ReflectionMethod;

class MetaMethod extends AbstractHasDocBlock
{

    /** @var ReflectionMethod */
    protected $reflected;
    /** @var string */
    protected $name;
    /** @var MetaParameter[] */
    protected $parameters;
    /** @var string */
    protected $returnType;
    /** @var string */
    protected $visibility;
    /** @var bool */
    protected $isStatic;

    public function __construct(ReflectionMethod $method)
    {
        parent::__construct($method);
        $this->name = $method->getShortName();
        $this->isStatic = $method->isStatic();
        $this->loadReturnType();
        $this->loadParameters();
        $this->loadVisibility();
    }

    private function loadReturnType()
    {
        if (is_null($this->reflected->getReturnType())) {
            if (!is_null($this->docblock) && $this->docblock->hasTag('return')) {
                $this->returnType = $this->docblock->getTagsByName('return')[0]->__toString();
            }
        } else {
            $this->returnType = $this->reflected->getReturnType()->__toString();
        }
    }

    private function loadParameters()
    {
        $this->parameters = new ArrayIterator();
        foreach ($this->reflected->getParameters() as $parameter) {
            $type = $description = '';
            if (!is_null($this->docblock)) {
                $paramDocBlock = $this->docblock->getTagsByName('param');
                foreach ($paramDocBlock as $doc) {
                    $line = explode(' ', trim($doc->__toString()));
                    if ($line[1] === '$' . $parameter->getName()) {
                        $type = $line[0];
                        $description = implode(' ', array_slice($line, 2));
                    }
                }
            }
            $this->parameters->append(new MetaParameter($parameter, $type, $description));
        }
    }

    private function loadVisibility()
    {
        $this->visibility =
            $this->reflected->isPrivate() ? 'privado' :
            $this->reflected->isProtected() ? 'protegido' :
            'público';
    }

    /**
     * Get the value of name
     *
     * @return  mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of parameters
     *
     * @return  MetaParameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Get the value of returnType
     *
     * @return  mixed
     */
    public function getReturnType()
    {
        return $this->returnType;
    }

    /**
     * Get the value of visibility
     *
     * @return  mixed
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Get the value of isStatic
     *
     * @return  mixed
     */
    public function isStatic()
    {
        return $this->isStatic;
    }
}
