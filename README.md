# LogicamenteDocumentorBundle

No `composer.json` do projeto,  adicionar:

```json
"repositories": [{
    "type": "gitlab",
    "url": "https://gitlab.com/logicamenteinfo/documentor-bundle.git"            
}]
```

Depois efetuar a requisição do projeto:

```bash
$ composer require --dev logicamenteinfo/documentor-bundle
```