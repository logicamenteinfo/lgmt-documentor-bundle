<?php

namespace Logicamente\DocumentorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('logicamente_documentor');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('services')
                    ->children()
                        ->arrayNode('LogicamenteDocumentorCommand')
                            ->children()
                                ->scalarNode('class')->defaultValue('Logicamente\DocumentorBundle\Command\DocumentorCommand')->end()
                                ->arrayNode('tags')
                                    ->children()
                                        ->scalarNode('name')->defaultValue('console.command')->end()
                                    ->end()
                                ->end()
                            ->end()                            
                        ->end()
                    ->end()
                ->end()
            ->end();
        return $treeBuilder;
    }
}
